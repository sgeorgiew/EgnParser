﻿using EgnParser.Models;
using EgnParser.Tools;
using System;

namespace EgnParser
{
    /// <summary>
    /// Provides information and allows you to inspect an Unified Civil Numbers (EGN)
    /// </summary>
    public class EgnInformator
    {
        /// <summary>
        /// Checks whether the given EGN is valid or not
        /// </summary>
        /// <param name="egn">Unified Civil Number (EGN), contains 10 digits</param>
        /// <returns>Returns true for valid EGN and false for invalid</returns>
        public bool Validate(string egn)
        {
            return Parser.Validate(egn);
        }

        /// <summary>
        /// Gets citizen information from given EGN
        /// </summary>
        /// <param name="egn">Unified Civil Number (EGN), contains 10 digits</param>
        /// <returns>Returns a citizen</returns>
        public Citizen GetCitizen(string egn)
        {
            return Parser.GetCitizen(egn);
        }
    }
}
